CXXFLAGS = -Wall -O3

all: paraclu

paraclu: paraclu.cc Makefile
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(LDFLAGS) -o $@ paraclu.cc

clean:
	rm -f paraclu

tag:
	git tag -m "" `git rev-list HEAD^ | grep -c .`
